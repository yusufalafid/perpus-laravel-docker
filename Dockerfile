FROM 10.210.210.10:5000/ubuntu:18.04

ARG DBHOST
ARG DBPORT
ARG DBUSERNAME
ARG DBPASSWORD
ARG DBDATABASE

RUN apt update -y && \
    apt install -y tzdata && \
    ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime && \
    apt install -y apache2 \
    php7.2 \
    libapache2-mod-php7.2 \
    php7.2-mbstring \
    php7.2-mysql \
    mysql-client \
    php7.2-xml \
    php7.2-gd \
    php7.2-dev \
    php7.2-cli \
    git \
    unzip \
    curl \
    libcurl4 \
    php7.2-curl \
    php-pear \
    libmcrypt-dev \
    libreadline-dev iputils-ping vim nano mysql-client
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer

RUN pecl install mcrypt-1.0.1 && \
    echo extension=/usr/lib/php/20170718/mcrypt.so > /etc/php/7.2/cli/conf.d/20-mcrypt.ini && \
    echo extension=/usr/lib/php/20170718/mcrypt.so > /etc/php/7.2/apache2/conf.d/20-mcrypt.ini

RUN mkdir /var/www/perpus
ADD . /var/www/perpus
ADD perpus.conf /etc/apache2/sites-available/
RUN a2enmod rewrite ssl && \
    a2dissite 000-default.conf && \
    a2ensite perpus.conf
WORKDIR /var/www/perpus

RUN composer install
RUN composer update
RUN cp .env.example .env
RUN php artisan key:generate
RUN sed -i "s/DB_HOST=127.0.0.1/DB_HOST=${DBHOST}/g" /var/www/perpus/.env
RUN sed -i "s/DB_PORT=3306/DB_PORT=${DBPORT}/g" /var/www/perpus/.env
RUN sed -i "s/DB_USERNAME=homestead/DB_USERNAME=${DBUSERNAME}/g" /var/www/perpus/.env
RUN sed -i "s/DB_PASSWORD=secret/DB_PASSWORD=${DBPASSWORD}/g" /var/www/perpus/.env
RUN sed -i "s/DB_DATABASE=homestead/DB_DATABASE=${DBDATABASE}/g" /var/www/perpus/.env


RUN cat /var/www/perpus/.env

EXPOSE 80
RUN ./install.sh
RUN chown www-data:www-data /var/www/perpus -R 
RUN chmod -R 755 /var/www/perpus 
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
